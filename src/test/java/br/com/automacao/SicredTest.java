package br.com.automacao;

import io.restassured.http.ContentType;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class SicredTest {

// ***********CPF com Restrição ************************************************
    @Test
    public void testConsultaCpfComRestricao1() { //ok

        given()
                .pathParam("cpf", "97093236014")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao2() { //ok

        given()
                .pathParam("cpf", "60094146012")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao3() { //ok

        given()
                .pathParam("cpf", "84809766080")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao4() { //ok

        given()
                .pathParam("cpf", "62648716050")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao5() { //ok

        given()
                .pathParam("cpf", "26276298085")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao6() { //ok

        given()
                .pathParam("cpf", "01317496094")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao7() { //ok

        given()
                .pathParam("cpf", "55856777050")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao8() { //ok

        given()
                .pathParam("cpf", "19626829001")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao9() { //ok

        given()
                .pathParam("cpf", "24094592008")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testConsultaCpfComRestricao10() { //ok

        given()
                .pathParam("cpf", "58063164083")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    //**************** CPF sem Retrição *****************************************
    @Test
    public void testConsultaCpfSemRestricao() { //ok

        given()
                .pathParam("cpf", "05648474458")
                .when()
                .get("http://localhost:8080/api/v1/restricoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(204);
    }

    // Dois metódos para testar trazendo simulações e cadastro de simulações vazio ****************************
    @Test
    public void testRetornarSimulacoesExistentes() { //ok
        // configurar o caminho de acesso a api rest
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api";
      when()
                .get("/v1/simulacoes")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testSeNaoTiverSimulacoesExistentes() { //ok
        // configurar o caminho de acesso a api rest
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api";
        when()
                .get("/v1/simulacoes")
                .then().log().all()
                .assertThat()
                .statusCode(400);
    }

    // *********************************Cadastro de Simulação******************************
    @Test
    public void testCadastrarSimulacaoSucesso() {
        // configurar o caminho de acesso a api rest
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api";


        given().body("{\"nome\": \"Sanmara Alves\",\"cpf\": \"33333333333\",\"email\": \"sanmaraalvesjp@gmail.com\",\"valor\": 1200,\"parcelas\": 3,\"seguro\": true}").contentType(ContentType.JSON)
                .when()
                .post("/v1/simulacoes")
                .then().log().all()
                .assertThat()
                .statusCode(201);
    }

    @Test
    public void testCadastrarSimulacaoCpfDuplicado() {
        // configurar o caminho de acesso a api rest
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api";
        given().body("{\"nome\": \"\",\"cpf\": \"\",\"email\": \"sanmaraalvesjp@gmail.com\",\"valor\": ,\"parcelas\": ,\"seguro\": true}").contentType(ContentType.JSON)
                .when()
                .post("/v1/simulacoes")
                .then().log().all()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void testCadastrarSimulacaoCpfJaExistente() {
        // configurar o caminho de acesso a api rest
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api";
        given().body("{\"nome\": \"Roberto Alves\",\"cpf\": \"07656704450\",\"email\": \"sanmaraalvesjp@gmail.com\",\"valor\": 1200,\"parcelas\": 3,\"seguro\": true}").contentType(ContentType.JSON)
                .when()
                .post("/v1/simulacoes")
                .then().log().all()
                .assertThat()
                .statusCode(409);
    }

     // *************** Retorna uma simulação através do CPF **********************

    @Test
    public void testRetornoSimulacaoCPF() { //ok

        given()
                .pathParam("cpf", "05648474458")
                .when()
                .get("http://localhost:8080/api/v1/simulacoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testRetornoSemSimulacaoCPF() { //ok

        given()
                .pathParam("cpf", "55555555555")
                .when()
                .get("http://localhost:8080/api/v1/simulacoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(404);
    }

    //************************** Atualiza uma simulação existente através do CPF *******************

    @Test
    public void testAtualizacaoDeSimulacao() { //ok


            // configurar o caminho de acesso a api rest
            baseURI = "http://localhost";
            port = 8080;
            basePath = "/api";
        given().body("{\"nome\": \"Fulano de Tal\",\"cpf\": \"97093236014\",\"email\": \"email@email.com\",\"valor\": 1200,\"parcelas\": 3,\"seguro\": true}").contentType(ContentType.JSON)
                .pathParam("cpf", "97093236014")
                    .when()
                    .put("/v1/simulacoes/{cpf}")
                    .then().log().all()
                    .assertThat()
                    .statusCode(200);

    }


    @Test
    public void testAtualizacaoDeSimulacaoNaoEncontrada() { //ok


        // configurar o caminho de acesso a api rest
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api";
        given().body("{\"nome\": \"Fulano de Tal\",\"cpf\": \"66666666666\",\"email\": \"email@email.com\",\"valor\": 1200,\"parcelas\": 3,\"seguro\": true}").contentType(ContentType.JSON)
                .pathParam("cpf", "66666666666")
                .when()
                .put("/v1/simulacoes/{cpf}")
                .then().log().all()
                .assertThat()
                .statusCode(404);

    }


    //************************** Remove uma simulação existente através do CPF *********************

    @Test
    public void testRemoverSimulacaoPorId() {
        delete("http://localhost:8080/api/v1/simulacoes/{id}", 1).
                then()
                .log().all()
                .assertThat()
                .statusCode(200);


    }

}
